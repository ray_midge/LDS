Feature: As a DBA
	I should be able to perform CRUD operations
	So that I know the database is robust
Scenario: create
	When I try to create a loan or pool
	Then object gets persisted to record
Scenario: read
	When I try to view a loan or pool
	Then object returns values via getter methods
Scenario: update
	When I try to edit a cell in the loan or pool
	Then object change gets reflected in database record 
Scenario: associate loan
	When I associate a loan with a pool 
	Then the FK in loan is set and amount is added to pool
Scenario: disassociate loan
	When I disassociate a loan with a pool 
	Then the FK in loan is set to null and amount is subtracted from pool
Scenario: delete
	When I try to delete a loan or pool
	Then object gets removed from database 