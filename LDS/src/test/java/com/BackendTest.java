package com;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;

import com.beans.Commitment;
import com.beans.Loan;
import com.model.OracleLoanDAO;
import com.model.OraclePoolDAO;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BackendTest {
	
	static Logger logger = Logger.getRootLogger();
	OracleLoanDAO loanDao = new OracleLoanDAO();
	OraclePoolDAO poolDao = new OraclePoolDAO();
	static int loanId; 
	static int poolId;
	
	
	@When("^I try to create a loan or pool$")
	public void i_try_to_create_a_loan_or_pool() throws Throwable {
		//create a loan
		Loan loan = new Loan("ARM", 3, "CITIMORTGAGE", null, new BigDecimal(1000.00), new Date(), 'Y');
		loanDao.createLoan(loan);
		loanId = loan.getLoanId();
		//create a pool
		Commitment pool = new Commitment("ARM", 3, "CITIMORTGAGE");
		poolDao.createPool(pool);
		poolId = pool.getPoolId();
		//by some strange hibernate quirk, id value is one less than actual value in DB, so -
		poolId += 1; loanId +=1;
	}

	@Then("^object gets persisted to record$")
	public void object_gets_persisted_to_record() throws Throwable {
	    Assert.assertEquals("CITIMORTGAGE", loanDao.readLoan(loanId).getLender());
	    Assert.assertEquals("CITIMORTGAGE", poolDao.readPool(poolId).getLender());
	    logger.info("UNIT TEST CREATE COMPLETE");
	}

	@When("^I try to view a loan or pool$")
	public void i_try_to_view_a_loan_or_pool() throws Throwable {
	}

	@Then("^object returns values via getter methods$")
	public void object_returns_values_via_getter_methods() throws Throwable {
	    Assert.assertEquals("ARM", loanDao.readLoan(loanId).getLoanType());
	    Assert.assertEquals("ARM", poolDao.readPool(poolId).getLoanType());
	    logger.info("UNIT TEST READ COMPLETE");
	}

	@When("^I try to edit a cell in the loan or pool$")
	public void i_try_to_edit_a_cell_in_the_loan_or_pool() throws Throwable {
	    loanDao.updateLoan(loanId, "FIXED", 4, "CHASE", new BigDecimal(1200.00));
	    poolDao.updatePool(poolId, "FIXED", 7, "BANK OF AMERICA");
	}

	@Then("^object change gets reflected in database record$")
	public void object_change_gets_reflected_in_database_record() throws Throwable {
		Assert.assertEquals("FIXED", loanDao.readLoan(loanId).getLoanType());
		Assert.assertEquals("BANK OF AMERICA", poolDao.readPool(poolId).getLender());
		logger.info("UNIT TEST UPDATE COMPLETE");
	}

	@When("^I associate a loan with a pool$")
	public void i_associate_a_loan_with_a_pool() throws Throwable {
	    loanDao.associateLoan(poolId, loanId);
	}

	@Then("^the FK in loan is set and amount is added to pool$")
	public void the_FK_in_loan_is_set_and_amount_is_added_to_pool() throws Throwable {
		logger.debug("FK object: " + loanDao.readLoan(loanId).getPool().toString());
	    Assert.assertEquals(loanDao.readLoan(loanId).getPool().getPoolId(), 
	    		poolDao.readPool(poolId).getPoolId());
	    logger.info("UNIT TEST ASSOCIATE COMPLETE");
	}

	@When("^I disassociate a loan with a pool$")
	public void i_disassociate_a_loan_with_a_pool() throws Throwable {
		loanDao.disassociateLoan(loanId);
	}

	@Then("^the FK in loan is set to null and amount is subtracted from pool$")
	public void the_FK_in_loan_is_set_to_null_and_amount_is_subtracted_from_pool() throws Throwable {
		Assert.assertNull(loanDao.readLoan(loanId).getPool());
	}
	
	@When("^I try to delete a loan or pool$")
	public void i_try_to_delete_a_loan_or_pool() throws Throwable {
	    loanDao.deleteLoan(loanDao.readLoan(loanId));
	    poolDao.deletePool(poolDao.readPool(poolId));
	}

	@Then("^object gets removed from database$")
	public void object_gets_removed_from_database() throws Throwable {
	    Assert.assertNull(loanDao.readLoan(loanId));
	    Assert.assertNull(poolDao.readPool(poolId));
	    logger.info("UNIT TEST DELETE COMPLETE");
	}
	
	
	
	

}
