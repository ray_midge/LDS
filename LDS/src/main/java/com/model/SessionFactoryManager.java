package com.model;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryManager {
	
	
	private static SessionFactory sessionFactory;
	
	private SessionFactoryManager(){
		// cannot put the following because 'sessionFactory' is an interface - 
		// sessionFactory = new sessionFactory();
		
		sessionFactory = 
				new Configuration().configure().buildSessionFactory();
		
		
		
	}
	
	
	// singleton accessor method
	public static SessionFactory getSessionFactory(){
		if(sessionFactory==null){
			new SessionFactoryManager();
			return sessionFactory;
		}else return sessionFactory;
	}
	

}
