package com.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.beans.Loan;
import com.beans.Commitment;

public class OraclePoolDAO {
	
	static Logger logger = Logger.getRootLogger();
	
	private SessionFactory sessionFactory;
	private Session session;

	public OraclePoolDAO() {

		sessionFactory = SessionFactoryManager.getSessionFactory();

	}
	
	
	public void createPool(Commitment pool) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(pool);
		tx.commit();
		session.close();
		logger.info("pool #" + pool.getPoolId() + " created");
	}
	
	
	public List<Commitment> readPools(){
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
    	String sql = "SELECT * FROM commitments WHERE is_open = 'Y'";
    	Query q = session.createSQLQuery(sql).addEntity(Commitment.class);
    	List<Commitment> pools = q.list();
    	tx.commit();
		session.close();
		return pools;
	}
	
	public Commitment readPool(int poolId){
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Commitment pool = (Commitment) session.get(Commitment.class, poolId);
		tx.commit();
		session.close();
		return pool;
	}
	
	public void updatePool(int poolId, String loanType, int riskLevel, String lender) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		String hql = "UPDATE Commitment SET loan_type = :loanType, risk_level = :riskLevel,"
				+ " lender = :lender WHERE pool_id = :ID";
		Query q = session.createQuery(hql);
		q.setParameter("ID", poolId);
		q.setParameter("loanType", loanType);
		q.setParameter("riskLevel", riskLevel);
		q.setParameter("lender", lender);
		q.executeUpdate();
		tx.commit();
		session.close();
		logger.info("pool #" + poolId + " updated");
	}
	

	public void deletePool(Commitment pool) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		OracleLoanDAO dao = new OracleLoanDAO();
		Set<Loan> loans = pool.getLoans();
		for (Loan loan : loans) dao.disassociateLoan(loan.getLoanId());

		session.delete(pool);

		tx.commit();
		session.close();
		logger.info("pool #" + pool.getPoolId() + " deleted");
	}
	
	public void packagePool(Commitment pool) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int poolId = pool.getPoolId();
//		String closeLoans = "UPDATE Loan SET active = 'N' WHERE pool = :ID";
//		Query qL = session.createQuery(closeLoans);
		String closeLoans = "UPDATE Loans SET active = 'N' WHERE pool = :ID";
		Query qL = session.createSQLQuery(closeLoans).addEntity(Loan.class);
		String closePool = "UPDATE Commitment SET is_open = 'N' WHERE pool_id = :ID";
		Query qP = session.createQuery(closePool);
		qL.setParameter("ID", poolId);
		qP.setParameter("ID", poolId);
		qL.executeUpdate();
		qP.executeUpdate();
		tx.commit();
		session.close();
		logger.info("pool #" + poolId + " has been packaged and closed");
	}
	

	


}
