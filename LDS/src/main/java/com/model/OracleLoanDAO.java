package com.model;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.beans.*;

public class OracleLoanDAO {

	static Logger logger = Logger.getRootLogger();

	private SessionFactory sessionFactory;
	private Session session;

	public OracleLoanDAO() {
		sessionFactory = SessionFactoryManager.getSessionFactory();
	}

	public void createLoan(Loan loan) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(loan);
		tx.commit();
		session.close();
		logger.info("loan #" + loan.getLoanId() + " created");
	}

	public Loan readLoan(int loanId) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Loan loan = (Loan) session.get(Loan.class, loanId);
		tx.commit();
		session.close();
		return loan;
	}
	
	public List<Loan> readLoans() {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String sql = "SELECT * FROM loans WHERE active = 'Y'";
		Query q = session.createSQLQuery(sql).addEntity(Loan.class);
		List<Loan> loans = q.list();
		tx.commit();
		session.close();
		return loans;
	}
	
	public List<Loan> readUnassociatedLoans() {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String sql = "SELECT * FROM loans WHERE pool IS NULL";
		Query q = session.createSQLQuery(sql).addEntity(Loan.class);
		List<Loan> loans = q.list();
		tx.commit();
		session.close();
		return loans;
	}

	public void updateLoan(int loanId, String loanType, int riskLevel, String lender, BigDecimal amt) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "UPDATE Loan SET loan_type = :loanType, risk_level = :riskLevel,"
				+ " lender = :lender, amount = :amount WHERE loan_id = :ID";
		// native sql apperently doesn't allow bulk manipulation..
//		String sql = "UPDATE loans SET loan_type=:loanType, risk_level=:riskLevel,"
//				+ " lender=:lender, amount=:amount, WHERE loan_id=:ID";
		Query q = session.createQuery(hql);
		//Query q = session.createSQLQuery(sql).addEntity(Loan.class);
		q.setParameter("ID", loanId);
		q.setParameter("loanType", loanType);
		q.setParameter("riskLevel", riskLevel);
		q.setParameter("lender", lender);
		q.setParameter("amount", amt);
		q.executeUpdate();
		tx.commit();
		session.close();
		logger.info("loan #" + loanId + " updated");
	}

	public void deleteLoan(Loan loan) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		session.delete(loan);

		tx.commit();
		session.close();
		System.out.println("loan #" + loan.getLoanId() + " deleted");
	}

	///////////////////////////////////////////////////

	public void associateLoan(int poolId, int loanId){
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		Loan loan = (Loan) session.get(Loan.class, loanId);
		Commitment pool = (Commitment) session.get(Commitment.class, poolId);
		pool.setAmount(pool.getAmount().add(loan.getAmount()));
		loan.setPool(pool);
		tx.commit();
		session.close();
		logger.info("pool #" + pool.getPoolId() + " associated with loan #" + loan.getLoanId());
	}

	public void disassociateLoan(int loanId) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int poolId;
		Loan loan = (Loan) session.get(Loan.class, loanId);

		try {
			poolId = loan.getPool().getPoolId();
			// }catch(SQLException e){e.printStackTrace();
		} catch (NullPointerException e) {
			logger.error("loan #" + loanId + " is not associated with any pool!");
			e.printStackTrace();
			return;
		}

		Commitment pool = (Commitment) session.get(Commitment.class, poolId);
		pool.setAmount(pool.getAmount().subtract(loan.getAmount()));
		loan.setPool(null);
		tx.commit();
		session.close();
		logger.info("pool #" + pool.getPoolId() + " disassociated from loan #" + loan.getLoanId());
	}

	public void disassociateLoanSetNull(int loanId) {
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String sql = "UPDATE loans SET pool=null WHERE loan_id=:ID";
		Query q = session.createSQLQuery(sql).addEntity(Loan.class);
		q.setParameter("ID", loanId);
		q.executeUpdate();
		tx.commit();
		session.close();
		logger.info("loan #" + loanId + " disassociated from its pool");
	}

	// public void updateLoan2(int loanId, BigDecimal amt){
	// session = sessionFactory.openSession();
	// Transaction tx = session.beginTransaction();
	// String sql = "UPDATE loans SET amount=:newAmount WHERE loan_id=:ID";
	// Query q = session.createSQLQuery(sql).addEntity(Loan.class);
	// q.setParameter("newAmount", amt);
	// q.setParameter("ID", loanId);
	// q.executeUpdate();
	// tx.commit();
	// session.close();
	// System.out.println("loan #" + loanId + " updated. new amount = " + amt +
	// "$");
	// }

}
