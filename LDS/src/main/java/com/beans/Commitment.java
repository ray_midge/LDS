package com.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "COMMITMENTS")
public class Commitment implements Serializable, Comparable<Commitment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4464126591378261186L;

	@Id
	@SequenceGenerator(name = "mySeq2", sequenceName = "seq_commitments", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mySeq2")
	@Column(name = "POOL_ID")
	private int poolId;

	@Column(name = "LOAN_TYPE")
	private String loanType;

	@Column(name = "RISK_LEVEL")
	private int riskLevel;

	@Column(name = "LENDER")
	private String lender;

	@Column(name = "IS_OPEN")
	private char isOpen;

	@Column(name = "AMOUNT")
	private BigDecimal amount;

	@Column(name = "DATE_PACKAGED")
	@Temporal(TemporalType.DATE)
	private Date packagedDate;

	@OneToMany(mappedBy = "pool", fetch=FetchType.EAGER)
	private Set<Loan> Loans;

	public Commitment() {
		super();
	}

	public Commitment(int poolId, String loanType, int riskLevel, String lender) {
		super();
		this.poolId = poolId;
		this.loanType = loanType;
		this.riskLevel = riskLevel;
		this.lender = lender;
		// commitments are initially open
		this.isOpen = 'Y';
		// commitments are made with initial value of zero
		this.amount = new BigDecimal(0);
		// this.packagedDate = packagedDate;
	}

	public Commitment(String loanType, int riskLevel, String lender) {
		super();
		this.loanType = loanType;
		this.riskLevel = riskLevel;
		this.lender = lender;
		this.isOpen = 'Y';
		// commitments are initially open
		this.isOpen = 'Y';
		// commitments are made with initial value of zero
		this.amount = new BigDecimal(0);
		// this.packagedDate = packagedDate;
	}

	public int getPoolId() {
		return poolId;
	}

	public void setPoolId(int poolId) {
		this.poolId = poolId;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public int getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getLender() {
		return lender;
	}

	public void setLender(String lender) {
		this.lender = lender;
	}

	public char getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(char isOpen) {
		this.isOpen = isOpen;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getPackagedDate() {
		return packagedDate;
	}

	public void setPackagedDate(Date packagedDate) {
		this.packagedDate = packagedDate;
	}

	public Set<Loan> getLoans() {
		return Loans;
	}

	public void setLoans(Set<Loan> loans) {
		Loans = loans;
	}
	
	
	public int compareTo(Commitment otherPool) {
		if(this.poolId > otherPool.poolId) return 1;
		else if(this.poolId < otherPool.poolId) return -1;
		else return 0;
	}

	@Override
	public String toString() {
		return "Commitment [poolId=" + poolId + ", loanType=" + loanType + ", riskLevel=" + riskLevel + ", lender="
				+ lender + ", amount=" + amount + ", date packaged=" + packagedDate + ", isOpen=" + isOpen + ", Loans="
				+ Loans + "]";
	}

}
