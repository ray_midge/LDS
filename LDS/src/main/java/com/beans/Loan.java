package com.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "LOANS")
public class Loan implements Serializable, Comparable<Loan> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6742478459361014968L;

	@Id
	@SequenceGenerator(name = "mySeq", sequenceName = "seq_loans", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mySeq")
	@Column(name = "LOAN_ID")
	private int loanId;

	@Column(name = "LOAN_TYPE")
	private String loanType;

	@Column(name = "RISK_LEVEL")
	private int riskLevel;

	@Column(name = "LENDER")
	private String lender;

	@ManyToOne(fetch = FetchType.EAGER) // (cascade=CascadeType.ALL)
	@JoinColumn(name="POOL")
	private Commitment pool;
	
	@Column(name="AMOUNT")
	private BigDecimal amount;
	
	@Column(name="DATE_INSERTED")
	@Temporal(TemporalType.DATE)
	private Date loanDate;

	@Column(name = "ACTIVE")
	private char active;

	public Loan() {
		super();
	}

	public Loan(String loanType, int riskLevel, String lender, Commitment poolId, 
			BigDecimal amount, Date loanDate, char active) {
		super();
		//this.loanId = loanId;
		this.loanType = loanType;
		this.riskLevel = riskLevel;
		this.lender = lender;
		this.pool = poolId;
		this.amount = amount;
		this.loanDate = loanDate;
		this.active = active;
	}
	
	public Loan(int loanId, String loanType, int riskLevel, String lender, Commitment poolId, 
			BigDecimal amount, Date loanDate, char active) {
		super();
		this.loanId = loanId;
		this.loanType = loanType;
		this.riskLevel = riskLevel;
		this.lender = lender;
		this.pool = poolId;
		this.amount = amount;
		this.loanDate = loanDate;
		this.active = active;
	}

	public int getLoanId() {
		return loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public int getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getLender() {
		return lender;
	}

	public void setLender(String lender) {
		this.lender = lender;
	}

	public Commitment getPool() {
		return pool;
	}

	public void setPool(Commitment poolId) {
		this.pool = poolId;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public Date getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}
	
	
	public int compareTo(Loan otherLoan) {
		if(this.loanId > otherLoan.loanId) return 1;
		else if(this.loanId < otherLoan.loanId) return -1;
		else return 0;
	}

	//omitted pool info because it could be null..
	@Override
	public String toString() {
		return "Loan [loanId=" + loanId + ", loanType=" + loanType + ", riskLevel=" + riskLevel + ", lender=" + lender
				+ ", amount=" + amount + ", loanDate=" + loanDate + ", active=" + active + "]";
	}


	

}
