package com.servlets;


import com.model.OracleLoanDAO;
import com.model.OraclePoolDAO;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.beans.*;

public class Stub {

	public static void main(String[] args) {

		OracleLoanDAO oracle = new OracleLoanDAO();
		OraclePoolDAO poolDao = new OraclePoolDAO();
		
		
		
	
		
		
		//oracle.updateLoan(30, new BigDecimal(6780));
		
		//select all from table loans
		List<Loan> loans = oracle.readLoans();
		Collections.sort(loans);
		for(Loan loan : loans){
		System.out.println(loan.getLoanId());
		}
		
//		int loanId, poolId;
//		Loan loan = new Loan("ARM", 3, "CITIMORTGAGE", null, new BigDecimal(1000.00), new Date(), 'Y');
//		oracle.createLoan(loan);
//		loanId = loan.getLoanId();
//		//create a pool
//		Commitment pool = new Commitment("ARM", 3, "CITIMORTGAGE");
//		poolDao.createPool(pool);
//		poolId = pool.getPoolId();
//		System.out.println("loanId:" + loanId + ", poolId:" + poolId);
		
		
		
		//create a loan
//		Date date = new Date(); 
//		Loan l = new Loan("FIXED", 3, "CHASE", null, new BigDecimal(7500.00), date, 'Y');
//		oracle.createLoan(l);
		
		
		//create a pool
//		Commitment pool = new Commitment("FIXED", 3, "CHASE");
//		poolDao.createPool(pool);
		
		
		//set pool/loan association
		//oracle.associateLoan(8, 48);
		
		//disassociate pool from loan
		//oracle.disassociateLoan(34);
		
		//set pool to null on loan object (FORCE DISASSOCIATE.. FOR TESTING)
		//oracle.disassociateLoanSetNull(48);

		
		
		//read loans from pool
//		Commitment pool = poolDao.readPool(8);
//		Set<Loan> loans = pool.getLoans();
//		for (Loan l : loans){
//			System.out.println(l.toString());
//		}
		
		
		//read loan
//		Loan loan = oracle.readLoan(48);
//		System.out.println(loan.toString());

		
	}
}
