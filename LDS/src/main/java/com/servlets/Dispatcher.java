package com.servlets;

import com.model.*;
import com.beans.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Dispatcher extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// force the GET request to make a POST request -
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String helper = req.getParameter("posting");
		System.out.println(helper);

		// Loan-handling cases -

		if (helper.equals("Add Loan")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			String lender = req.getParameter("lender");
			String loanType = req.getParameter("loanType");
			int riskLevel = Integer.parseInt(req.getParameter("riskLevel"));
			try {
				long amount = Long.parseLong(req.getParameter("amount"));
				Loan loan = new Loan(loanType, riskLevel, lender, null, new BigDecimal(amount), new Date(), 'Y');
				loanDao.createLoan(loan);
				String m = "new loan added. thank you.";
				req.getSession().removeAttribute("errorMessage");
				req.getSession().setAttribute("message", m);
				resp.sendRedirect("/LDS/main.jsp");
			} catch (NumberFormatException e) {
				String m = "input for new loan amount is not a valid number";
				req.getSession().removeAttribute("message");
				req.getSession().setAttribute("errorMessage", m);
				resp.sendRedirect("/LDS/main.jsp");
			}
		}

		else if (helper.equals("Delete Loan")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			int loanId = Integer.parseInt(req.getParameter("del"));
			Loan loan = loanDao.readLoan(loanId);
			loanDao.deleteLoan(loan);
			String m = "loan #" + loanId + " deleted.";
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			resp.sendRedirect("/LDS/main.jsp");
		}

		else if (helper.equals("Edit Loan")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			int loanId = Integer.parseInt(req.getParameter("ed"));
			Loan loan = loanDao.readLoan(loanId);
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("edit", loan);
			req.getSession().setAttribute("type", "Loan");
			resp.sendRedirect("/LDS/edit.jsp");
		}

		else if (helper.equals("Submit Loan Edit")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			int loanId = Integer.parseInt(req.getParameter("editsub"));
			String lender = req.getParameter("lender");
			String loanType = req.getParameter("loanType");
			int riskLevel = Integer.parseInt(req.getParameter("riskLevel"));
			try {
				long amount = Long.parseLong(req.getParameter("amount"));
				loanDao.updateLoan(loanId, loanType, riskLevel, lender, new BigDecimal(amount));
				req.getSession().removeAttribute("errorMessage");
				String m = "successfully edited loan #" + loanId + ".";
				req.getSession().setAttribute("message", m);
				resp.sendRedirect("/LDS/main.jsp");
			} catch (NumberFormatException e) {
				String m = "input for amount is not a valid number";
				req.getSession().removeAttribute("message");
				req.getSession().setAttribute("errorMessage", m);
				resp.sendRedirect("/LDS/edit.jsp");
			}
		}

		////////////////////////////////////////////////////////////////

		// Pool-handling cases -

		else if (helper.equals("Add Pool")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			String lender = req.getParameter("lender");
			String loanType = req.getParameter("loanType");
			int riskLevel = Integer.parseInt(req.getParameter("riskLevel"));
			Commitment pool = new Commitment(loanType, riskLevel, lender);
			poolDao.createPool(pool);
			String m = "new pool created. thank you.";
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			resp.sendRedirect("/LDS/main.jsp");
		}

		else if (helper.equals("Delete Pool")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = Integer.parseInt(req.getParameter("del"));
			Commitment pool = poolDao.readPool(poolId);
			poolDao.deletePool(pool);
			String m = "pool #" + poolId + " deleted.";
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			resp.sendRedirect("/LDS/main.jsp");
		}

		else if (helper.equals("Edit Pool")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = Integer.parseInt(req.getParameter("ed"));
			Commitment pool = poolDao.readPool(poolId);
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("edit", pool);
			req.getSession().setAttribute("type", "Pool");
			resp.sendRedirect("/LDS/edit.jsp");
		}

		else if (helper.equals("Submit Pool Edit")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = Integer.parseInt(req.getParameter("editsub"));
			String lender = req.getParameter("lender");
			String loanType = req.getParameter("loanType");
			int riskLevel = Integer.parseInt(req.getParameter("riskLevel"));
			poolDao.updatePool(poolId, loanType, riskLevel, lender);
			String m = "successfully edited pool #" + poolId + ".";
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			resp.sendRedirect("/LDS/main.jsp");
		}

		else if (helper.equals("Detail")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			OracleLoanDAO loanDao = new OracleLoanDAO();
			int poolId = Integer.parseInt(req.getParameter("detail"));
			Commitment pool = poolDao.readPool(poolId);
			List<Loan> ucLoans = loanDao.readUnassociatedLoans();
			Set<Loan> pLoansSet = pool.getLoans();
			List<Loan> pLoans = new ArrayList<Loan>(pLoansSet);
			Collections.sort(ucLoans);
			Collections.sort(pLoans);
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("poolId", poolId);
			req.getSession().setAttribute("pool", pool);
			req.getSession().setAttribute("ucLoans", ucLoans);
			req.getSession().setAttribute("pLoans", pLoans);
			resp.sendRedirect("/LDS/pool.jsp");
		}

		else if (helper.equals("Associate")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = (Integer) req.getSession().getAttribute("poolId");
			int loanId = Integer.parseInt(req.getParameter("associate"));

			Commitment pool = poolDao.readPool(poolId);
			Loan loan = loanDao.readLoan(loanId);
			// validation logic..
			if (pool.getLender().equals(loan.getLender()) && pool.getLoanType().equals(loan.getLoanType())) {
				loanDao.associateLoan(poolId, loanId);
				pool = poolDao.readPool(poolId);
				List<Loan> ucLoans = loanDao.readUnassociatedLoans();
				Set<Loan> pLoans = pool.getLoans();
				String m = "loan #" + loanId + " successfully associated with pool #" + poolId;
				req.getSession().removeAttribute("errorMessage");
				req.getSession().setAttribute("message", m);
				req.getSession().setAttribute("pool", pool);
				req.getSession().setAttribute("ucLoans", ucLoans);
				req.getSession().setAttribute("pLoans", pLoans);
				resp.sendRedirect("/LDS/pool.jsp");
			} else {
				String m = "criteria parameters 'lender' and 'loan type'" 
						+ " in loan #" + loanId + " do not match those of pool #" + poolId
						+ ". please check to make sure that loan and pool are compatible.";
				req.getSession().removeAttribute("message");
				req.getSession().setAttribute("errorMessage", m);
				resp.sendRedirect("/LDS/pool.jsp");
			}
		}

		else if (helper.equals("Disassociate")) {
			OracleLoanDAO loanDao = new OracleLoanDAO();
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = (Integer) req.getSession().getAttribute("poolId");
			int loanId = Integer.parseInt(req.getParameter("disassociate"));
			loanDao.disassociateLoan(loanId);
			Commitment pool = poolDao.readPool(poolId);
			List<Loan> ucLoans = loanDao.readUnassociatedLoans();
			Set<Loan> pLoans = pool.getLoans();
			String m = "loan #" + loanId + " successfully disassociated with pool #" + poolId;
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			req.getSession().setAttribute("pool", pool);
			req.getSession().setAttribute("ucLoans", ucLoans);
			req.getSession().setAttribute("pLoans", pLoans);
			resp.sendRedirect("/LDS/pool.jsp");
		}

		else if (helper.equals("Package Pool")) {
			OraclePoolDAO poolDao = new OraclePoolDAO();
			int poolId = (Integer) req.getSession().getAttribute("poolId");
			Commitment pool = poolDao.readPool(poolId);
			poolDao.packagePool(pool);
			String m = "pool #" + poolId + " has been packaged and closed. thank you.";
			req.getSession().removeAttribute("errorMessage");
			req.getSession().setAttribute("message", m);
			resp.sendRedirect("/LDS/main.jsp");
		}

	}

}
