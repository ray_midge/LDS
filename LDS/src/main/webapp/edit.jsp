<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.beans.Loan"
	import="com.model.OracleLoanDAO" import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%!String flag;%>
<%
	String flag = (String) session.getAttribute("type");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="styler.css" rel="stylesheet" type="text/css" />
<title>Edit Page</title>
</head>
<body>
<span class="errorMessage">${errorMessage}</span>
	<br>
	<p>Edit ${type}</p>
	<div id="panel_create">
		<form action="Servlets/hello.do" id="addLoan" method="post">
			<table id="regTable">
				<tr>
					<td><label for="lender">Lender: </label></td>
					<td><select name="lender">
							<option value="CHASE">JP Morgan Chase</option>
							<option value="BANK OF AMERICA">Bank of America</option>
							<option value="WELLS FARGO">Wells Fargo</option>
							<option value="CITIMORTGAGE">CitiMortgage</option>
							<option value="QUICKEN LOANS">Quicken Loans</option>
					</select></td>
					<td><label id="lenderLabel"></label>
				</tr>
				<tr>
					<td><label for="loanType">Fixed or ARM: </label></td>
					<td><input type="radio" name="loanType" value="FIXED" checked>Fixed
						<input type="radio" name="loanType" value="ARM">ARM</td>
					<td><label id="loanTypeLabel"></label>
				<tr>
				<tr>
					<td><label for="riskLevel">Risk level (between 1 and 5): </label></td>
					<td><select name="riskLevel">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
					</select></td>
					<td><label id="riskLevelLabel"></label>
				<tr>
					<%
						if (flag.equals("Loan")) {
					%>
				
				<tr>
					<td><label for="amount">Amount: </label></td>
					<td><input type="text" id="amount" name="amount"
						value="${edit.amount}" /></td>
					<td><label id="amountLabel"></label></td>
				</tr>
				<%
					}
				%>
			</table>
			<%
				if (flag.equals("Loan")) {
			%>
			<input type="hidden" name="editsub" value="${edit.loanId}" /> <input
				type="submit" name="posting" value="Submit Loan Edit">
			<%
				} else {
			%>
			<input type="hidden" name="editsub" value="${edit.poolId}" /> <input
				type="submit" name="posting" value="Submit Pool Edit">
			<%
				}
			%>

		</form>
	</div>
</body>
</html>