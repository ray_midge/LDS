<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.beans.*" import="com.model.*"
	import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%!OracleLoanDAO loanDao = new OracleLoanDAO();
	OraclePoolDAO poolDao = new OraclePoolDAO();
	List<Loan> loans;
	List<Commitment> pools;%>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LDS Home Page</title>
<link href="styler.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="external.js"></script>

</head>
<%
	loans = loanDao.readLoans();
	pools = poolDao.readPools();
	Collections.sort(loans);
	Collections.sort(pools);
	session = request.getSession();
	session.setAttribute("loans", loans);
	session.setAttribute("pools", pools);
%>
<body>
	<span class="message">${message}</span>
	<span class="errorMessage">${errorMessage}</span>
	<br>
	<p>Loan access:</p>
	<br>
	<div id="flipCreateLoan">click here to fill out a loan form</div>
	<div id=panelCreateLoan>
		<form action="Servlets/hello.do" id="addLoan" method="post">
			<table id="loanCreateTable">
				<tr>
					<td><label for="lender">Lender: </label></td>
					<td><select name="lender">
							<option value="CHASE">JP Morgan Chase</option>
							<option value="BANK OF AMERICA">Bank of America</option>
							<option value="WELLS FARGO">Wells Fargo</option>
							<option value="CITIMORTGAGE">CitiMortgage</option>
							<option value="QUICKEN LOANS">Quicken Loans</option>
					</select></td>
					<td><label id="lenderLabel"></label>
				</tr>
				<tr>
					<td><label for="loanType">Fixed or ARM: </label></td>
					<td><input type="radio" name="loanType" value="FIXED" checked>Fixed
						<input type="radio" name="loanType" value="ARM">ARM</td>
					<td><label id="loanTypeLabel"></label>
				<tr>
				<tr>
					<td><label for="riskLevel">Risk level: </label></td>
					<td><select name="riskLevel">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
					</select></td>
					<td><label id="riskLevelLabel"></label>
				<tr>
				<tr>
					<td><label for="amount">Amount: </label></td>
					<td><input type="text" id="amount" name="amount" /></td>
					<td><label id="amountLabel"></label>
			</table>
			<input type="submit" name="posting" value="Add Loan">
		</form>
	</div>

	<br>

	<div id="flipReadLoan">click here to see current loans</div>
	<div id="panelReadLoan">
		<table>
			<tr>
				<th>Loan ID</th>
				<th>Lender</th>
				<th>Amount</th>
				<th>Loan Type</th>
				<th>Date Created</th>
				<th />
			<tr>
				<c:forEach var="loan" items="${loans}">
					<tr>
						<td><c:out value="${loan.loanId}"></c:out></td>
						<td><c:out value="${loan.lender}"></c:out></td>
						<td><c:out value="\$${loan.amount}"></c:out></td>
						<td><c:out value="${loan.loanType}"></c:out></td>
						<td><c:out value="${loan.loanDate}"></c:out></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" name="del" value="${loan.loanId}" /> <input
									type="submit" name="posting" value="Delete Loan">
							</form></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" name="ed" value="${loan.loanId}" /> <input
									type="submit" name="posting" value="Edit Loan">
							</form></td>
					</tr>
				</c:forEach>
		</table>
	</div>
	<!-- -------------------------LOAN/POOL SEPERATION---------------------------- -->
	<br>
	<br>
	<p>Pool access:</p>
	<br>

	<div id="flipCreatePool">click here to fill out a pool form</div>
	<div id="panelCreatePool">
		<form action="Servlets/hello.do" id="addPool" method="post">
			<table id="poolCreateTable">
				<tr>
					<td><label for="lender">Lender: </label></td>
					<td><select name="lender">
							<option value="CHASE">JP Morgan Chase</option>
							<option value="BANK OF AMERICA">Bank of America</option>
							<option value="WELLS FARGO">Wells Fargo</option>
							<option value="CITIMORTGAGE">CitiMortgage</option>
							<option value="QUICKEN LOANS">Quicken Loans</option>
					</select></td>
					<td><label id="lenderLabel"></label>
				<tr>
					<td><label for="loanType">Fixed or ARM: </label></td>
					<td><input type="radio" name="loanType" value="FIXED" checked>Fixed
						<input type="radio" name="loanType" value="ARM">ARM</td>
					<td><label id="loanTypeLabel"></label>
				<tr>
					<td><label for="riskLevel">Risk level: </label></td>
					<td><select name="riskLevel">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
					</select></td>
					<td><label id="riskLevelLabel"></label>
			</table>
			<input type="submit" name="posting" value="Add Pool">
		</form>
	</div>

	<br>

	<div id="flipReadPool">click here to see current pools</div>
	<div id="panelReadPool">
		<table>
			<tr>
				<th>Pool ID</th>
				<th>Lender</th>
				<th>Loan Type</th>
				<th />
				<th />
			<tr>
				<c:forEach var="pool" items="${pools}">
					<tr>
						<td><c:out value="${pool.poolId}"></c:out></td>
						<td><c:out value="${pool.lender}"></c:out></td>
						<td><c:out value="${pool.loanType}"></c:out></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" name="del" value="${pool.poolId}" /> <input
									type="submit" name="posting" value="Delete Pool">
							</form></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" name="ed" value="${pool.poolId}" /> <input
									type="submit" name="posting" value="Edit Pool">
							</form></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" name="detail" value="${pool.poolId}" /> <input
									type="submit" name="posting" value="Detail">
							</form></td>
					</tr>
				</c:forEach>
		</table>
		<span>(you can associate/disassociate loans from a pool and
			submit a pool for packaging using the 'Detail' button)</span>
	</div>
	<br>




</body>
</html>