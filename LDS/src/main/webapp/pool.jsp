<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.beans.*" import="com.model.*"
	import="java.util.List" import="java.math.BigDecimal"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%!	Commitment pool; %>
<%	pool = (Commitment) session.getAttribute("pool");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="styler.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="external.js"></script>
<title>Pool View</title>
</head>
<body>
	<a href="/LDS/main.jsp">home page</a>
	<br>
	<p>information for pool #${pool.poolId}:</p>
	<div>
		lender: ${pool.lender}<br> loan type: ${pool.loanType}<br>
		risk level: ${pool.riskLevel}<br> <span id="amt">total amount in loan:
		\$${pool.amount}</span><br>
	</div>
	<br>
	<span class="message">${message}</span>
	<span class="errorMessage">${errorMessage}</span>
	<br>
	<div id="flipReadPool">click here to see loans associated with
		pool #${pool.poolId}</div>
	<div id="panelReadPool">
		<table>
			<tr>
				<th>Loan ID</th>
				<th>Lender</th>
				<th>Amount</th>
				<th>Loan Type</th>
				<th>Risk Level</th>
			<tr>
				<c:forEach var="loan" items="${pLoans}">
					<tr>
						<td><c:out value="${loan.loanId}"></c:out></td>
						<td><c:out value="${loan.lender}"></c:out></td>
						<td><c:out value="\$${loan.amount}"></c:out></td>
						<td><c:out value="${loan.loanType}"></c:out></td>
						<td><c:out value="${loan.riskLevel}"></c:out></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" id="disassociate" name="disassociate"
									value="${loan.loanId}"> <input type="submit"
									name="posting" value="Disassociate">
							</form></td>
					</tr>
				</c:forEach>
		</table>
	</div>
	<br>
	<br>
	<!-- ----------------------------------------------------------------------------- -->

	<div id="flipReadLoan">click here to see loans eligible for
		association</div>
	<div id="panelReadLoan">
		<table>
			<tr>
				<th>Loan ID</th>
				<th>Lender</th>
				<th>Amount</th>
				<th>Loan Type</th>
				<th>Risk Level</th>
			<tr>
				<c:forEach var="loan" items="${ucLoans}">
					<tr>
						<td><c:out value="${loan.loanId}"></c:out></td>
						<td><c:out value="${loan.lender}"></c:out></td>
						<td><c:out value="\$${loan.amount}"></c:out></td>
						<td><c:out value="${loan.loanType}"></c:out></td>
						<td><c:out value="${loan.riskLevel}"></c:out></td>
						<td><form action="/LDS/hello.do" method="POST">
								<input type="hidden" id="associate" name="associate"
									value="${loan.loanId}"> <input type="submit"
									name="posting" value="Associate">
							</form></td>
					</tr>
				</c:forEach>
		</table>
	</div>
	<br>
	<br>
	<%  
	int flag = (pool.getAmount().compareTo(new BigDecimal(30000)));
	if(flag >= 0){ %>
	<span>please click button to submit pool for packaging into financial instrument.</span>
	<form action="Servlets/hello.do" id="packagePool" method="post">
		<input type="submit" name="posting" value="Package Pool">
			</form>
	<%} else{ %>
		<span>pool amount must be at least $30000 to be packaged</span><br>
		<button type="button" disabled>Package Pool</button>
	<%} %>
</body>
</html>