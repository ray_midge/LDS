Feature: As a mortgage broker
	I want to perform all the necessary LDS functionalities
	Via the web user interface
Scenario Outline: create
	When I fill out the loan or pool using <Loan Type>, <Lender>, and <Amount>
	Then The new loan or pool will be created
Examples: 
	| Loan Type | Lender        | Amount |
	| "ARM"     | "CHASE"       | 4600   |
#	| "FIXED"   | "WELLS FARGO" | 16500  |
Scenario: read
	When i try to read a loan or pool
	Then the loans and pools display on the main page
Scenario: update
	When i try to edit a loan or pool
	Then the loan changes values
Scenario: delete
	When i try to delete a loan or pool
	Then the loan or pool becomes deleted
Scenario: associate loan
	When i try to associate a loan to a pool
	Then loan amount gets added to pool amount
Scenario: disassociate loan
	When i try to disassociate a loan from a pool
	Then loan amount gets subtracted from pool amount






	