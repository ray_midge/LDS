package com.LDSweb;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class FrontendTest {
	
	private WebDriver driver;
	private WebElement element;
	private String id, amt;
	private float poolAmt, loanAmt;

	
	
	@When("^I fill out the loan or pool using \"([^\"]*)\", \"([^\"]*)\", and (\\d+)$")
	public void i_fill_out_the_loan_or_pool_using_and(String arg1, String arg2, int arg3) throws Throwable {
		
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		
		// create loan - 
		element = driver.findElement(By.id("flipCreateLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[1]/td[2]/select/option[3]"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[2]/td[2]/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[6]/td[2]/input"));
		element.sendKeys("6700");
		element = driver.findElement(By.xpath("//*[@id=\"addLoan\"]/input"));
		element.click();

		// create pool - 
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		element = driver.findElement(By.id("flipCreatePool"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"poolCreateTable\"]/tbody/tr[1]/td[2]/select/option[3]"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"poolCreateTable\"]/tbody/tr[2]/td[2]/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"addPool\"]/input"));
		element.click();
		
		//throw new PendingException();
	}

	@Then("^The new loan or pool will be created$")
	public void the_new_loan_or_pool_will_be_created() throws Throwable {
		//see if latest loan addition equals arbitrary property set above e.g. $6700 -
		element = driver.findElement(By.id("flipReadLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[3]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		Assert.assertEquals("$6700", element.getText());
	    //for pool - 
		element = driver.findElement(By.id("flipReadPool"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadPool\"]/table/tbody/tr[last()]/td[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		Assert.assertEquals("WELLS FARGO", element.getText());
		
		driver.close();
	}

	@When("^i try to read a loan or pool$")
	public void i_try_to_read_a_loan_or_pool() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		
		//throw new PendingException();
	}

	@Then("^the loans and pools display on the main page$")
	public void the_loans_and_pools_display_on_the_main_page() throws Throwable {
		Assert.assertNotNull(driver.findElement(By.id("flipReadPool")));
		Assert.assertNotNull(driver.findElement(By.id("flipReadLoan")));
		
		driver.close();
	}

	@When("^i try to edit a loan or pool$")
	public void i_try_to_edit_a_loan_or_pool() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		element = driver.findElement(By.id("flipReadLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[7]/form/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"regTable\"]/tbody/tr[6]/td[2]/input"));
		element.clear();
		element.sendKeys("8900");
		element = driver.findElement(By.xpath("//*[@id=\"addLoan\"]/input[2]"));
		element.click();
		
		//throw new PendingException();
	}
	

	@Then("^the loan changes values$")
	public void the_loan_changes_values() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		element = driver.findElement(By.id("flipReadLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[1]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[3]"));
		Assert.assertEquals("$8900", element.getText());
		
		driver.close();
	}

	@When("^i try to delete a loan or pool$")
	public void i_try_to_delete_a_loan_or_pool() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		element = driver.findElement(By.id("flipReadLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[1]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		id = element.getText();
		System.out.println(id);
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[6]/form/input[2]"));
		element.click();
		
		//throw new PendingException();
	}

	@Then("^the loan or pool becomes deleted$")
	public void the_loan_or_pool_becomes_deleted() throws Throwable {
		element = driver.findElement(By.id("flipReadLoan"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[1]"));
		Assert.assertNotEquals(id, element.getText());
		System.out.println(element.getText());
		
		driver.close();
	}
	

	@When("^i try to associate a loan to a pool$")
	public void i_try_to_associate_a_loan_to_a_pool() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		
		// create loan - 
		element = driver.findElement(By.id("flipCreateLoan"));
		element.click();
		// select "bank of america" - 
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[1]/td[2]/select/option[2]"));
		element.click();
		// select type "ARM" - 
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[2]/td[2]/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		// select amount "4600" - 
		element = driver.findElement(By.xpath("//*[@id=\"loanCreateTable\"]/tbody/tr[6]/td[2]/input"));
		element.sendKeys("4600");
		element = driver.findElement(By.xpath("//*[@id=\"addLoan\"]/input"));
		element.click();
		
		// create pool - 
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipCreateLoan")));
		element = driver.findElement(By.id("flipCreatePool"));
		element.click();
		// select "bank of america" - 
		element = driver.findElement(By.xpath("//*[@id=\"poolCreateTable\"]/tbody/tr[1]/td[2]/select/option[2]"));
		element.click();
		// select type "ARM" - 
		element = driver.findElement(By.xpath("//*[@id=\"poolCreateTable\"]/tbody/tr[2]/td[2]/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"addPool\"]/input"));
		element.click();
		
		//go to edit.jsp
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipReadPool")));
		(driver.findElement(By.id("flipReadPool"))).click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadPool\"]/table/tbody/tr[last()]/td[last()]/form/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		
		//make sure initially equals 0 dollars - 
		element = driver.findElement(By.xpath("//*[@id=\"amt\"]"));
		amt = (element.getText()).substring(22);
		Assert.assertEquals("$0", amt);
		
		//associate loan - 
		element = driver.findElement(By.xpath("//*[@id=\"flipReadLoan\"]"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadLoan\"]/table/tbody/tr[last()]/td[last()]/form/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		
		
		//throw new PendingException();
	}

	@Then("^loan amount gets added to pool amount$")
	public void loan_amount_gets_added_to_pool_amount() throws Throwable {
		//make sure pool amount  now = $4600
		element = driver.findElement(By.xpath("//*[@id=\"amt\"]"));
		String amt = (element.getText()).substring(22);
		Assert.assertEquals("$4600", amt);
		
		driver.close();
	}

	@When("^i try to disassociate a loan from a pool$")
	public void i_try_to_disassociate_a_loan_from_a_pool() throws Throwable {
		File file = new File("C:/Users/Chris/Documents/Java/selenium-2.48.2/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://52.34.201.38:7001/LDS");
		
		//go to edit.jsp
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("flipReadPool")));
		(driver.findElement(By.id("flipReadPool"))).click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadPool\"]/table/tbody/tr[last()]/td[last()]/form/input[2]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		element.click();
		
		//get initial pool amount - 
		element = driver.findElement(By.xpath("//*[@id=\"amt\"]"));
		amt = (element.getText()).substring(23);
		poolAmt = Float.parseFloat(amt);
		
		//disassociate
		element = driver.findElement(By.xpath("//*[@id=\"flipReadPool\"]"));
		element.click();
		element = driver.findElement(By.xpath("//*[@id=\"panelReadPool\"]/table/tbody/tr[last()]/td[3]"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
		//get amount before disassociating - 
		amt = (element.getText()).substring(1);
		loanAmt = Float.parseFloat(amt);
		(driver.findElement(By.xpath("//*[@id=\"panelReadPool\"]/table/tbody/tr[last()]/td[last()]/form/input[2]"))).click();
		
		
	    //throw new PendingException();
	}

	@Then("^loan amount gets subtracted from pool amount$")
	public void loan_amount_gets_subtracted_from_pool_amount() throws Throwable {
		//expected  pool amount - 
		float actualAmt = poolAmt - loanAmt;
		//get actual new pool amount - 
		element = driver.findElement(By.xpath("//*[@id=\"amt\"]"));
		amt = (element.getText()).substring(23);
		poolAmt = Float.parseFloat(amt);
		Assert.assertEquals(actualAmt, poolAmt, 0.001);
		
		driver.close();
	}



	



}
